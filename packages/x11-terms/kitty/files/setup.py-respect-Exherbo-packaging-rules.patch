Upstream: no

From 44b4e5f42c7e86ae393db2dc098f88c7b6626fd9 Mon Sep 17 00:00:00 2001
From: Alexander Kapshuna <kapsh@kap.sh>
Date: Thu, 14 Mar 2024 12:14:29 +0000
Subject: [PATCH] setup.py: respect Exherbo packaging rules

---
 setup.py | 39 ++++++++++++++++++++++++---------------
 1 file changed, 24 insertions(+), 15 deletions(-)

diff --git a/setup.py b/setup.py
index d9d608264..29200af8d 100755
--- a/setup.py
+++ b/setup.py
@@ -53,7 +53,7 @@
 is_arm = platform.processor() == 'arm' or platform.machine() in ('arm64', 'aarch64')
 Env = glfw.Env
 env = Env()
-PKGCONFIG = os.environ.get('PKGCONFIG_EXE', 'pkg-config')
+PKGCONFIG = os.environ.get('PKG_CONFIG', 'pkg-config')
 link_targets: List[str] = []
 macos_universal_arches = ('arm64', 'x86_64') if is_arm else ('x86_64', 'arm64')
 
@@ -457,7 +457,7 @@ def init_env(
         df += ' -Og'
         float_conversion = '-Wfloat-conversion'
     fortify_source = '' if sanitize and is_macos else '-D_FORTIFY_SOURCE=2'
-    optimize = df if debug or sanitize else '-O3'
+    optimize = ''
     sanitize_args = get_sanitize_args(cc, ccver) if sanitize else []
     cppflags_ = os.environ.get(
         'OVERRIDE_CPPFLAGS', '-D{}DEBUG'.format('' if debug else 'N'),
@@ -487,7 +487,7 @@ def init_env(
     )
     ldflags_ = os.environ.get(
         'OVERRIDE_LDFLAGS',
-        '-Wall ' + ' '.join(sanitize_args) + ('' if debug else ' -O3')
+        '-Wall ' + ' '.join(sanitize_args)
     )
     ldflags = shlex.split(ldflags_)
     ldflags.append('-shared')
@@ -762,10 +762,7 @@ def dependecies_for(src: str, obj: str, all_headers: Iterable[str]) -> Iterable[
 
 
 def parallel_run(items: List[Command]) -> None:
-    try:
-        num_workers = max(2, os.cpu_count() or 1)
-    except Exception:
-        num_workers = 2
+    num_workers = int(os.environ.get('EXJOBS', os.cpu_count()))
     items = list(reversed(items))
     workers: Dict[int, Tuple[Optional[Command], Optional['subprocess.Popen[bytes]']]] = {}
     failed = None
@@ -1192,7 +1189,7 @@ def build_launcher(args: Options, launcher_dir: str = '.', bundle_type: str = 's
     if bundle_type.startswith('macos-'):
         klp = '../Resources/kitty'
     elif bundle_type.startswith('linux-'):
-        klp = '../{}/kitty'.format(args.libdir_name.strip('/'))
+        klp = os.path.join(os.path.relpath(args.libdir_name, args.bindir_name), 'kitty')
     elif bundle_type == 'source':
         klp = os.path.relpath('.', launcher_dir)
     elif bundle_type == 'develop':
@@ -1256,8 +1253,8 @@ def copy_man_pages(ddir: str) -> None:
             shutil.copy2(y, os.path.join(mandir, f'man{x}'))
 
 
-def copy_html_docs(ddir: str) -> None:
-    htmldir = os.path.join(ddir, 'share', 'doc', appname, 'html')
+def copy_html_docs(ddir: str, docdir) -> None:
+    htmldir = os.path.join(ddir, docdir, 'html')
     safe_makedirs(os.path.dirname(htmldir))
     with suppress(FileNotFoundError):
         shutil.rmtree(htmldir)
@@ -1274,6 +1271,7 @@ def copy_html_docs(ddir: str) -> None:
 def compile_python(base_path: str) -> None:
     import compileall
     import py_compile
+    num_workers = int(os.environ.get('EXJOBS', os.cpu_count()))
     for root, dirs, files in os.walk(base_path):
         for f in files:
             if f.rpartition('.')[-1] in ('pyc', 'pyo'):
@@ -1281,15 +1279,17 @@ def compile_python(base_path: str) -> None:
 
     exclude = re.compile('.*/shell-integration/ssh/bootstrap.py')
     compileall.compile_dir(
-        base_path, rx=exclude, force=True, optimize=(0, 1, 2), quiet=1, workers=0,  # type: ignore
+        base_path, rx=exclude, force=True, optimize=(0, 1, 2), quiet=1, workers=num_workers,  # type: ignore
         invalidation_mode=py_compile.PycInvalidationMode.UNCHECKED_HASH, ddir='')
 
 
 def create_linux_bundle_gunk(ddir: str, args: Options) -> None:
+    bindir_name = args.bindir_name
     libdir_name = args.libdir_name
+    docdir_name = args.docdir_name
     base = Path(ddir)
     in_src_launcher = base / (f'{libdir_name}/kitty/kitty/launcher/kitty')
-    launcher = base / 'bin/kitty'
+    launcher = base / bindir_name / 'bin/kitty'
     skip_docs = False
     if not os.path.exists('docs/_build/html'):
         kitten_exe = os.path.join(os.path.dirname(str(launcher)), 'kitten')
@@ -1307,7 +1307,7 @@ def create_linux_bundle_gunk(ddir: str, args: Options) -> None:
                 raise SystemExit(f'kitten binary not found at: {kitten_exe}')
     if not skip_docs:
         copy_man_pages(ddir)
-        copy_html_docs(ddir)
+        copy_html_docs(ddir, docdir_name)
     for (icdir, ext) in {'256x256': 'png', 'scalable': 'svg'}.items():
         icdir = os.path.join(ddir, 'share', 'icons', 'hicolor', icdir, 'apps')
         safe_makedirs(icdir)
@@ -1590,7 +1590,7 @@ def create_macos_bundle_gunk(dest: str, for_freeze: bool, args: Options) -> str:
     with open(ddir / 'Contents/Info.plist', 'wb') as fp:
         fp.write(macos_info_plist())
     copy_man_pages(str(ddir))
-    copy_html_docs(str(ddir))
+    copy_html_docs(str(ddir), docdir_name)
     os.rename(ddir / 'share', ddir / 'Contents/Resources')
     os.rename(ddir / 'bin', ddir / 'Contents/MacOS')
     os.rename(ddir / 'lib', ddir / 'Contents/Frameworks')
@@ -1619,7 +1619,7 @@ def package(args: Options, bundle_type: str, do_build_all: bool = True) -> None:
     libdir = os.path.join(ddir, args.libdir_name.strip('/'), 'kitty')
     if os.path.exists(libdir):
         shutil.rmtree(libdir)
-    launcher_dir = os.path.join(ddir, 'bin')
+    launcher_dir = os.path.join(ddir, args.bindir_name)
     safe_makedirs(launcher_dir)
     if for_freeze:  # freeze launcher is built separately
         if do_build_all:
@@ -1803,6 +1803,15 @@ def option_parser() -> argparse.ArgumentParser:  # {{{
         default=Options.dir_for_static_binaries,
         help='Where to create the static kitten binary'
     )
+    p.add_argument(
+        '--bindir-name',
+        default='bin',
+        help='The name of the directory inside --prefix in which to store compiled files. Defaults to "bin"'
+    )
+    p.add_argument(
+        '--docdir-name',
+        help='The name of the directory inside --prefix in which to store documentation'
+    )
     p.add_argument(
         '--skip-code-generation',
         default=Options.skip_code_generation,
-- 
2.44.0

