# Copyright 2017 Tom Briden
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'passwordsafe-1.02.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

require github [ user=pwsafe project=pwsafe ]
require cmake
require wxwidgets
require gtk-icon-cache
require utf8-locale

SUMMARY="popular secure and convenient password manager"
HOMEPAGE+=" https://pwsafe.org"

LICENCES="Artistic-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    qr          [[ description = [ Show password as QR code ] ]]
    xml         [[ description = [ Enable support for importing from XML ] ]]
    yubikey     [[ description = [ Enable support for YubiKey 2FA ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        net-misc/curl
        sys-apps/file
        sys-apps/util-linux
        x11-libs/libX11
        x11-libs/libXtst
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        qr? ( media-libs/qrencode:= )
        yubikey? ( sys-auth/yubikey-personalization )
        xml? ( dev-libs/xerces-c )
        !app-crypt/pwsafe [[
            description = [ Provides the same binary and is outdated, use pwsafe-cli instead ]
            resolution = uninstall-blocked-before
        ]]
    test:
        dev-cpp/gtest
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DGTEST_BUILD:BOOL=FALSE
    -DPWS_BUILD_HELP:BOOL=TRUE
    -DUSE_ASAN:BOOL=FALSE
    -DUSE_INTERPROCEDURAL_OPTIMIZATION:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    '!qr NO_QR'
    'xml XML_XERCESC'
    '!yubikey NO_YUBI'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DNO_GTEST:BOOL=FALSE -DNO_GTEST:BOOL=TRUE'
)

pkg_setup() {
    wxwidgets_pkg_setup

    # required for tests
    require_utf8_locale
}

src_prepare() {
    cmake_src_prepare

    # TODO: report upstream, prevent share from ending up under prefix
    edo sed \
        -e 's:DESTINATION "share:DESTINATION "${CMAKE_INSTALL_DATAROOTDIR}:g' \
        -i CMakeLists.txt
}

