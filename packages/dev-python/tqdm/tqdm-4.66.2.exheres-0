# Copyright 2018 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=setuptools test=pytest entrypoints=[ ${PN} ] ]

SUMMARY="Fast, Extensible Progress Meter"
DESCRIPTION="
Instantly make your loops show a smart progress meter - just wrap any iterable
with tqdm(iterable), and you’re done!
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm:0[>=3.4][python_abis:*(-)?]
    test:
        dev-python/numpy[python_abis:*(-)?]
        dev-python/pytest-asyncio[python_abis:*(-)?]
        dev-python/pytest-timeout[python_abis:*(-)?]
        dev-python/rich[python_abis:*(-)?]
"

PYTEST_PARAMS=(
    # Avoid some (unwritten) test dependencies
    --ignore=tests/tests_dask.py
    --ignore=tests/tests_keras.py
    --ignore=tests/tests_pandas.py
    --ignore=tests/tests_tk.py
)
PYTEST_SKIP=(
    # These two raise exceptions because they are apparently too slow.
    test_iter_overhead_simplebar_hard
    test_manual_overhead_simplebar_hard
)

